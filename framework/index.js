import { mailBoxCheck } from '../framework/services/index';

export const mailBoxApiProvider = () => ({
  mailBoxCheck: () => new mailBoxCheck(),
});
