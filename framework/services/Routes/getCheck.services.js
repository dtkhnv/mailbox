import supertest from 'supertest';
import { mailBoxLayerSetting } from '../../config/settings';

const request = supertest(mailBoxLayerSetting.url);

export const mailBoxCheck = function mailBoxCheck() {
  this.get = async function getMailBoxCheck(token, email) {
    const r = await request.get(`check?access_key=${token}&email=${email}`).set('Accept', 'application/json');

    return r;
  };
};
