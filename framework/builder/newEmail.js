import faker from 'faker';

export const personBuilder = () => {
  return {
    name: faker.name.findName(),
    email: faker.internet.email(),
  };
};
