/**
 * Используя апи провайдер и контроллеры напишите набор функци проверок для сервиса.
 * Первый тест - положительный.
 * Второй тест - набор параметризированных тестов.
 * Третий тест - проверяет права доступа к точке без api_key
 */

import { describe, expect, test } from '@jest/globals';
import { mailBoxApiProvider } from '../framework/index';
import { mailBoxLayerSetting } from '../framework/config/settings';
import { personBuilder } from '../framework/builder/newEmail';

describe('Тестируем сервис - https://mailboxlayer.com/', () => {
  test('Проверяем положительный тест', async () => {
    const { status, body } = await mailBoxApiProvider()
      .mailBoxCheck()
      .get(mailBoxLayerSetting.token, personBuilder().email);

    expect(status).toEqual(200);
    expect(body.email).not.toEqual('undefined');
    expect(body.format_valid).not.toEqual('false');
  }, 20000);
  test('Проверяем тест без передачи токена', async () => {
    const { status, body } = await mailBoxApiProvider().mailBoxCheck().get('token', personBuilder().email);

    expect(status).toEqual(200);
    expect(body.success).toEqual(false);
    expect(body.error.code).toEqual(101);
    expect(body.error.type).toEqual('invalid_access_key');
  }, 20000);
});
describe('Параметризованный тест', () => {
  test.each`
    name                        | email                    | bodyEx            | expected
    ${'format_valid = true'}    | ${personBuilder().email} | ${'format_valid'} | ${true}
    ${'format_valid = false'}   | ${personBuilder().name}  | ${'format_valid'} | ${false}
    ${'user = "test"'}          | ${'test@test.ru'}        | ${'user'}         | ${'test'}
    ${'email = "test@test.ru"'} | ${'test@test.ru'}        | ${'email'}        | ${'test@test.ru'}
    ${'domain = "test.ru"'}     | ${'test@test.ru'}        | ${'domain'}       | ${'test.ru'}
  `(
    'Проверяем параметризованеый тест: $name === $email',
    async ({ email, bodyEx, expected }) => {
      const { status, body } = await mailBoxApiProvider().mailBoxCheck().get(mailBoxLayerSetting.token, email);

      expect(status).toEqual(200);
      expect(body[bodyEx]).toEqual(expected);
    },
    20000
  );
});
